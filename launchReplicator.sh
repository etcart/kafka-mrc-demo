/usr/bin/replicator \
--cluster.id replicator \
--producer.config ~/single_region_example/producer.properties \
--consumer.config ~/single_region_example/consumer.properties \
--replication.config ./replicator.properties
